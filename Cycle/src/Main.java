
public class Main {
    public static void main(String[] args) {
        System.out.println("fibonacci with for: ");
        int num1 = 1;
        int num2 = 1;
        int temp;
        System.out.println("1: "+num1+"\n" + "2: "+num2);
        for(int i = 3;i<12;i++){
            temp = num1+num2;
            System.out.println(i+": "+temp);
            num1 = num2;
            num2 = temp;
        }

        System.out.println("fibonacci with while: ");
        num1 = 1;
        num2 = 1;
        int i = 3;
        System.out.println("1: "+num1+"\n" + "2: "+num2);
        while(i<12){
            temp = num1+num2;
            System.out.println(i+": "+temp);
            num1 = num2;
            num2 = temp;
            i++;
        }

    }
}